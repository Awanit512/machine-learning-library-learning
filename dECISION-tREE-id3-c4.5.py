import numpy as np

import math


def Calculate_Gain(I):
  s = np.sum(I)
  Log= [ -1*math.log(x/s,2) if x!=0 else 0 for x in I ] 
  Log = np.array(Log)

  # Log = -1*np.log10(I/s) / math.log(2,10)
  entropy =  np.sum((I*Log ))/ s
  return entropy 












classes = int(input("No: of classes: "))
print("Now Enter Complete value of matrix :\n ")
# print("\t\tCLASS_1  | CLASS_2 |")
# print("Branch-1            p\t  |    q \nBranch-2            r\t  |        q \nBranch-...            r\t  |    s \n \n\nPlease enter as p q r s ")

print("brach -value 1  | class 1  | class 3  | ...  | class K\nbrach -value 2 | class 1  | class 3  | ...  | class K  |\nbrach -value ...| class 1  | class 2  | ...  | class K ")

k = input()
print("\n\n")
k=[int(x) for x in k.split()]
#print(k)
total = np.sum(k)
A=[]



for i in range(0,len(k),classes):
  #print("i:",i)
  a=[]
  for j in range(i,classes+i):
    a.append( k[j])
  A.append(a)
#print("fffff")
#print(A,"------------------------")
branch = len(k)//classes
#print(branch)

#print(f"AAAAAAAAAAAAAAAA\n{A}\n\n\n\n\n\n")
G=[]
strr =[]
split_info = []
for i in range(branch):
  I = np.array(A[i])
  gain = Calculate_Gain(I)
  sum=np.sum(I)
  split_info.append(sum)
  G.append( (sum*gain)/total )
  s = "("+str(sum)+"/"+str(total)+")*"+str(gain)
  strr.append(s)
  print("|",end="")
  for j in range(len(A[i])):
    print(f"{A[i][j]} |",end="")
  print("] "+str(gain))
print("\n\n")




split_info = np.array(split_info)
split__info = Calculate_Gain(split_info)
h=[]
for i in list(zip(*A)):
  h.append(np.sum(i))


h = np.array(h)
entropy = Calculate_Gain(h)
for i,j in enumerate(strr):
  print(str(j)+" = "+str(G[i])+"\n")

print(f"\n\nInformation Gain = {(entropy).round(5)} - {(np.sum(G)).round(5)} = {(entropy - np.sum(G)).round(5)} ")
print(f"\n\nSplit_info :{(split__info).round(5)}","\n\n")

print(f"Gain Ratio = Information_Gain/Split_Info = {(entropy - np.sum(G)).round(5)}/ {(split__info).round(5) } = { ((entropy - np.sum(G))/split__info).round(5) } ")


